# Change log for trivtox, the trivial tox test runner

## 0.1.0 (2022-08-25)

- First public release

Comments: Peter Pentchev <[roam@ringlet.net](mailto:roam@ringlet.net)>
