"""The main trivtox program: parse tox.ini, run the tests."""

# Copyright (c) 2018, 2020, 2022  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

from __future__ import print_function

import argparse
import os
import subprocess
import sys

from trivtox import config
from trivtox import VERSION as VERSION_STRING


TOX_INI_DEFAULT = "tox.ini"


class InvalidSectionError(Exception):
    """Invalid contents of a tox.ini section."""


def version() -> None:
    """Display program version information."""
    print("trivtox " + VERSION_STRING)


def features() -> None:
    """Display a list of the features supported by the program."""
    print("Features: trivtox=" + VERSION_STRING)


def diag_stderr(text: str) -> None:
    """Display a diagnostic message to the standard error stream."""
    print(f"DIAG: {text}", file=sys.stderr)


def diag_none(_: str) -> None:
    """Do not display a diagnostic message."""


def main() -> None:
    """The main program: read the tox config file, run commands."""
    parser = argparse.ArgumentParser(
        prog="trivtox",
        usage="""
    trivtox [-Nv] [-a | -e envlist]
    trivtox -V | -h | --help | --version
    trivtox --features""",
    )
    parser.add_argument("-N", "--noop", action="store_true", help="no-operation mode")
    parser.add_argument(
        "-V",
        "--version",
        action="store_true",
        help="display program version information and exit",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="display diagnostic information",
    )
    parser.add_argument(
        "--features",
        action="store_true",
        help="display program feature information",
    )

    sect_envlist = parser.add_mutually_exclusive_group()
    sect_envlist.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="process all environments in envlist",
    )
    sect_envlist.add_argument(
        "-e", "--envlist", type=str, help="specify the environments to process"
    )

    args = parser.parse_args()
    if args.version:
        version()
        sys.exit(0)

    if args.features:
        features()
        sys.exit(0)

    diag = diag_stderr if args.verbose else diag_none
    fname = TOX_INI_DEFAULT

    diag(f"Processing {fname}")
    # Pass positional arguments through?
    try:
        if args.envlist is None:
            envs = None
        else:
            envs = config.split_envlist(args.envlist)
        cfg = config.parse(fname, "", process_all=args.all, process_envlist=envs)
    except config.ConfigError as err:
        sys.exit(str(err))

    diag(
        # pylint: disable-next=consider-using-f-string
        "Processing {cnt} section{s}: {sect}".format(
            cnt=len(cfg.envlist),
            s="" if len(cfg.envlist) == 1 else "s",
            sect=" ".join(cfg.envlist),
        )
    )

    errors = False
    for name in cfg.envlist:
        try:
            env = cfg.environments[name]
            diag(str(env))

            environ = dict(os.environ)
            environ.update(env.envvars)

            for cmd in env.commands:
                diag(f"run: {cmd}")
                if args.noop:
                    print(cmd)
                else:
                    subprocess.check_call([cmd], env=environ, shell=True)
        except Exception as err:  # pylint: disable=broad-except
            print(f"Could not process section {name}: {err}", file=sys.stderr)
            errors = True

    sys.exit(1 if errors else 0)


if __name__ == "__main__":
    main()
