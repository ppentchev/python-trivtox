"""Parse the tox configuration file."""

# Copyright (c) 2018, 2020, 2022  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

from __future__ import annotations

import configparser
import dataclasses
import pathlib
import re


@dataclasses.dataclass(frozen=True)
class ConfigError(Exception):
    """A base error class."""

    filename: str
    error: Exception

    @property
    def action(self) -> str:
        """Return the action during which the error occurred."""
        raise NotImplementedError()

    def __str__(self) -> str:
        """Return human-readable information about the error."""
        return f"Could not {self.action} the {self.filename} file: {self.error}"


@dataclasses.dataclass(frozen=True)
class ReadError(ConfigError):
    """An error that occurred while reading the file."""

    @property
    def action(self) -> str:
        """This error occurred while reading the file."""
        return "read"


@dataclasses.dataclass(frozen=True)
class ParseError(ConfigError):
    """An error that occurred while parsing the file."""

    @property
    def action(self) -> str:
        """This error occurred while parsing the file."""
        return "parse"


@dataclasses.dataclass(frozen=True)
class ToxError(ConfigError):
    """An error that occurred while parsing the tox data."""

    @property
    def action(self) -> str:
        """This error is specific to the tox format."""
        return "parse the tox data in"


@dataclasses.dataclass(frozen=True)
class ToxSectionError(ToxError):
    """An error that occurred while parsing a tox section."""

    section: str

    @property
    def action(self) -> str:
        return f"parse the {self.section} section in"


@dataclasses.dataclass(frozen=True)
class Environment:
    """A single environment."""

    name: str
    commands: list[str]
    envvars: dict[str, str]

    def __str__(self) -> str:
        cmds = self.commands
        return f"Environment {self.name}: {len(cmds)} command{'' if len(cmds) == 1 else 's'}"


@dataclasses.dataclass
class Config:
    """The full configuration."""

    filename: str
    envlist: list[str] = dataclasses.field(default_factory=list)
    environments: dict[str, Environment] = dataclasses.field(default_factory=dict)

    def add_environment(self, name: str, commands: list[str], envvars: dict[str, str]) -> None:
        """Add an environment and its commands."""
        assert name not in self.envlist
        assert name not in self.environments

        env = Environment(name=name, commands=commands, envvars=envvars)

        self.envlist.append(name)
        self.environments[name] = env


RE_SUBST = re.compile(
    r"""
    ^
    (?P<prefix> .*? )

    [{]
    (?:
            [{]
                (?P<literal> [^}]* )
            [}]
        |
            (?:
                \[ (?P<section> [a-zA-Z0-9_]+ ) \]
            )?
            (?P<name> [a-zA-Z0-9_]+ )
    )
    [}]

    (?P<suffix> .* )
""",
    re.X,
)


def subst(
    filename: str,
    section: str,
    value: str,
    cfgp: configparser.ConfigParser,
    cache: dict[str, dict[str, str]],
) -> str:
    """Substitute the environment settings and tox variables in a string."""
    result = ""
    while True:
        match = RE_SUBST.match(value)
        if not match:
            if "{" in value:
                raise ToxSectionError(
                    filename=filename,
                    section=section,
                    error=Exception("Invalid substitution"),
                )
            return result + value

        values = match.groupdict()
        result += values["prefix"]
        value = values["suffix"]

        if values["literal"] is not None:
            result += values["literal"]
            continue

        sect = values["section"] if values["section"] is not None else ""
        name = values["name"]
        if sect in cache and name in cache[sect]:
            result += cache[sect][name]
            continue
        if sect == "":
            raise ToxSectionError(
                filename=filename,
                section=section,
                error=Exception(f"Invalid predefined variable {name}"),
            )
        try:
            sect_data = cfgp[sect]
        except KeyError as err:
            raise ToxSectionError(
                filename=filename,
                section=section,
                error=Exception(f"Invalid substitution section {sect}"),
            ) from err
        try:
            vvalue = sect_data[name]
        except KeyError as err:
            raise ToxSectionError(
                filename=filename,
                section=section,
                error=Exception(f"No {name} in {sect}"),
            ) from err
        vsubst = subst(filename, sect, vvalue, cfgp, cache)
        if sect not in cache:
            cache[sect] = {}
        cache[sect][name] = vsubst
        result += vsubst


def subst_envvars(
    fname: str,
    envname: str,
    data: configparser.SectionProxy,
    cfgp: configparser.ConfigParser,
    subst_cache: dict[str, dict[str, str]],
) -> dict[str, str]:
    """Substitute values in the "setenv" definitions of an environment."""
    if "setenv" not in data:
        return {}

    envvars = {}
    for line in data["setenv"].split("\n"):
        line = line.strip()
        if not line:
            continue
        if "=" not in line:
            raise ToxSectionError(
                filename=fname,
                section=envname,
                error=Exception("Invalid setenv line"),
            )
        name, value = line.split("=", 1)
        envvars[name.strip()] = subst(fname, envname, value.strip(), cfgp, subst_cache)

    return envvars


def split_envlist(envlist: str) -> list[str]:
    """Split an envlist specification into environment names."""
    return [name.strip() for name in envlist.strip().split(",")]


def parse(
    fname: str,
    posargs: str,
    process_all: bool = False,
    process_envlist: list[str] | None = None,
) -> Config:
    """Read a config file and return the environments."""

    def get_sections() -> list[str]:
        """Return the requested names from tox.envlist."""
        env_str = cfgp["tox"].get("envlist", None)
        if env_str is None:
            raise ToxError(filename=fname, error=Exception('no "envlist" setting'))
        envs = split_envlist(env_str)

        if process_envlist is not None:
            missing = [name for name in process_envlist if name not in envs]
            if missing:
                raise ToxError(
                    filename=fname,
                    error=Exception(
                        # pylint: disable-next=consider-using-f-string
                        "unknown environment{es}: {missing}".format(
                            es="" if len(missing) == 1 else "s",
                            missing=",".join(missing),
                        )
                    ),
                )
            return process_envlist

        if process_all:
            return envs

        return [sect for sect in envs if sect.startswith("pep8") or sect.startswith("mypy")]

    cfg = Config(filename=fname)

    try:
        cfgp = configparser.ConfigParser(interpolation=None)
        cfgp.read_file(
            pathlib.Path(fname).read_text(encoding="UTF-8").rstrip().split("\n"),
            fname,
        )
    except (IOError, OSError, FileNotFoundError) as err:
        raise ReadError(filename=fname, error=err) from err
    except configparser.Error as err:
        raise ParseError(filename=fname, error=err) from err

    if "tox" not in cfgp:
        raise ToxError(filename=fname, error=Exception('no "tox" section'))

    subst_cache = {
        "": {
            "posargs": posargs,
            "toxinidir": str(pathlib.Path(fname).parent.absolute()),
        },
    }

    for section in get_sections():
        envname = f"testenv:{section}"
        try:
            data = cfgp[envname]
        except KeyError as err:
            raise ToxSectionError(
                filename=fname,
                section=envname,
                error=Exception("No such section"),
            ) from err

        envvars = subst_envvars(fname, envname, data, cfgp, subst_cache)

        cmds = data.get("commands", None)
        if cmds is None:
            raise ToxSectionError(
                filename=fname,
                section=envname,
                error=Exception('No "commands" setting'),
            )

        commands = [
            subst(fname, envname, cmd.strip(), cfgp, subst_cache) for cmd in cmds.split("\n")
        ]
        cfg.add_environment(section, commands, envvars)

    return cfg
