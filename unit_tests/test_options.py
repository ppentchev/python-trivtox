"""Test the processing of command-line options."""

# Copyright (c) 2018, 2020, 2022  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


import re

import pytest

from . import utils


RE_SEMVER_PATTERN = r"""
           (?: 0 | [1-9][0-9]* )    # major
        \. (?: 0 | [1-9][0-9]* )    # minor
        \. (?: 0 | [1-9][0-9]* )    # patchlevel
    (?: \. [a-zA-Z0-9]+ )?          # optional addendum (dev1, beta3, etc.)
    """

RE_SEMVER = re.compile("^" + RE_SEMVER_PATTERN + "$", re.X)

RE_FEATURES_TRIVTOX = re.compile(
    r"""^
    Features: \s+
    .*?
    \b trivtox = """
    + RE_SEMVER_PATTERN
    + r"\b",
    re.X,
)


class TestOptions:
    """Test the output of trivtox run with simple options."""

    def test_unknown_option(self) -> None:
        """Test that trivtox breaks with unrecognized options."""
        with pytest.raises(utils.ErrorOutputError):
            utils.run_trivtox(["-X"])
        with pytest.raises(utils.ErrorOutputError):
            utils.run_trivtox(["-hX"])
        with pytest.raises(utils.ErrorOutputError):
            utils.run_trivtox(["--features", "-Y"])
        with pytest.raises(utils.ErrorOutputError):
            utils.run_trivtox(["-N", "--nah"])

    def test_help_version_features(self) -> None:
        """Test combinations of --help, --version, and --features."""
        res = utils.run_trivtox(["--version"])
        assert res.exitcode == 0
        assert len(res.stdout_lines) == 1
        fields = res.stdout_lines[0].split(" ")
        assert len(fields) == 2
        assert fields[0] == "trivtox"
        assert RE_SEMVER.match(fields[1])
        version = res.stdout_lines[0]

        res = utils.run_trivtox(["-V"])
        assert res.exitcode == 0
        assert res.stdout_lines == [version]

        res = utils.run_trivtox(["--help"])
        assert res.exitcode == 0
        assert len(res.stdout_lines) > 2
        fields = res.stdout_lines[0].split(" ")
        assert fields[0] == "usage:"
        fields = res.stdout_lines[1].split(" ")
        while fields and fields[0] == "":
            fields = fields[1:]
        assert fields[0] == "trivtox"
        help_lines = res.stdout_lines

        res = utils.run_trivtox(["-h"])
        assert res.exitcode == 0
        assert res.stdout_lines == help_lines

        res = utils.run_trivtox(["--features"])
        assert res.exitcode == 0
        assert len(res.stdout_lines) == 1
        assert RE_FEATURES_TRIVTOX.match(res.stdout_lines[0])
