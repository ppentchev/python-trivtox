"""Utility functions for the trivtox unit tests."""

# Copyright (c) 2018, 2020, 2022  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

from __future__ import annotations

import contextlib
import dataclasses
import os
import subprocess
import sys
import tempfile

from typing import Iterator


@dataclasses.dataclass(frozen=True)
class ChildResult:
    """The output and exit code of a child process."""

    exitcode: int
    stdout: str
    stderr: str

    stdout_lines: list[str] = dataclasses.field(init=False)
    stderr_lines: list[str] = dataclasses.field(init=False)

    def __post_init__(self) -> None:
        """Split the program's output into lines in various ways."""
        object.__setattr__(self, "stdout_lines", self.stdout.splitlines())
        object.__setattr__(self, "stderr_lines", self.stderr.splitlines())

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        # pylint: disable-next=consider-using-f-string
        return "exit code {code}, {out} line{os} of output, " "{err} error{es}".format(
            code=self.exitcode,
            out=len(self.stdout_lines),
            os="" if len(self.stdout_lines) == 1 else "s",
            err=len(self.stderr_lines),
            es="" if len(self.stderr_lines) == 1 else "s",
        )


@dataclasses.dataclass(frozen=True)
class ErrorOutputError(Exception):
    """The child process output something to its standard error stream."""

    res: ChildResult

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        # pylint: disable-next=consider-using-f-string
        return "The child process exited with code {code}:\n{errs}".format(
            code=self.res.exitcode,
            errs="\n".join(["    " + line for line in self.res.stderr_lines]),
        )


def run_trivtox(args: list[str], path: str = ".") -> ChildResult:
    """Run the trivtox script with the specified parameters."""

    def decode_or_empty(data: bytes | None) -> str:
        """Decode the bytes into a string if any."""
        if data is None:
            return ""
        return data.decode("UTF-8")

    pythonpath = os.environ.get("PYTHONPATH", None)
    if pythonpath is None:
        pythonpath = path
    else:
        pythonpath = path + ":" + pythonpath

    cmd = [
        "env",
        f"PYTHONPATH={pythonpath}",
        sys.executable,
        "-m",
        "trivtox",
    ] + args
    proc_res = subprocess.run(cmd, capture_output=True, check=False, shell=False)
    res = ChildResult(
        proc_res.returncode, decode_or_empty(proc_res.stdout), decode_or_empty(proc_res.stderr)
    )
    if proc_res.stderr:
        raise ErrorOutputError(res=res)
    return res


@contextlib.contextmanager
def chdir_into_temp() -> Iterator[tuple[str, str]]:
    """Create a temporary directory, change into it, then back."""
    with tempfile.TemporaryDirectory() as tempdir:
        cwd = os.getcwd()
        try:
            os.chdir(tempdir)
            yield cwd, tempdir
        finally:
            os.chdir(cwd)
