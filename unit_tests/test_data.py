"""Data for the trivtox tests."""

# Copyright (c) 2018, 2020  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


NO_TOX_SECTION = """
[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = ostestr {posargs}

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands =
  mypy --strict trivtox.py
  mypy --strict unit_tests
"""

NO_ENVLIST = """
[tox]
skipsdist = True

[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = ostestr {posargs}

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands =
  mypy --strict trivtox.py
  mypy --strict unit_tests
"""

NO_ENV = """
[tox]
envlist = mypy_2
skipsdist = True

[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = ostestr {posargs}

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands =
  mypy --strict trivtox.py
  mypy --strict unit_tests
"""

NO_COMMAND = """
[tox]
envlist = mypy_3,unit_tests_3
skipsdist = True

[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = ostestr {posargs}

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
"""

FAILED_COMMAND = """
[tox]
envlist = mypy_3,unit_tests_3
skipsdist = True

[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = ostestr {posargs}

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = false
"""

FAILED_ONE_OF_TWO = """
[tox]
envlist = pep8,mypy_3,unit_tests_3
skipsdist = True

[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = ostestr {posargs}

[testenv:pep8]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = true

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands =
  true
  false
"""

ECHOES = """
[tox]
envlist = pep8,mypy_2,mypy_3,unit_tests_3
skipsdist = True

[testenv:unit_tests_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = echo four

[testenv:pep8]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = echo one

[testenv:mypy_2]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands =
  true

[testenv:mypy_3]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands =
  echo three
  echo two
"""

NO_INTERPOLATION = """
[tox]
envlist = pep8,%(envname)s
envname = mypy_2
skipsdist = True

[testenv:pep8]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = echo good

[testenv:mypy_2]
basepython = python3
deps = -r{toxinidir}/test-requirements.txt
commands = echo bad
"""

SETENV = """
[tox]
envlist = pep8_setenv
skipsdist = True

[testenv:pep8_setenv]
basepython = python3
setenv =
  VERDICT = good
commands = python -c 'import os; print(os.environ["VERDICT"])'
"""

INVALID_SETENV = """
[tox]
envlist = pep8_setenv
skipsdist = True

[testenv:pep8_setenv]
basepython = python3
setenv =
  VERDICT = good
  something else
commands = python -c 'import os; print(os.environ["VERDICT"])'
"""

SUBST_FULL = """
[tox]
envlist = pep8_dumpvars
skipsdist = true
toxworkdir = {toxinidir}/obj/.tox
temp_dir = {toxinidir}/obj/.tmp

[sp]
lib = {toxinidir}/scripts/usr/lib/storpool
libpy = {[sp]lib}/python
tests = {toxinidir}/tests
unit = {[sp]tests}/unit
unitpy = {[sp]unit}/python
clean_py = {[sp]tests}/clean-py.sh {[sp]libpy} {[sp]unitpy}

[literal]
stuff = some {{stuff}} here

[testenv:pep8_dumpvars]
basepython = python3
deps = flake8
commands =
  echo 'top-toxinidir-{toxinidir}-whee'
  echo 'tox-toxworkdir-{[tox]toxworkdir}-whee'
  echo 'tox-temp_dir-{[tox]temp_dir}-whee'
  echo 'sp-lib-{[sp]lib}-whee'
  echo 'sp-libpy-{[sp]libpy}-whee'
  echo 'sp-tests-{[sp]tests}-whee'
  echo 'sp-unit-{[sp]unit}-whee'
  echo 'sp-unitpy-{[sp]unitpy}-whee'
  echo 'sp-clean_py-{[sp]clean_py}-whee'
  echo 'literal-stuff-{[literal]stuff}-whee'
"""

SUBST_BAD_LITERAL = """
[tox]
envlist = pep8

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {{literal}'
"""

SUBST_UNCLOSED = """
[tox]
envlist = pep8

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {toxinidir'
"""

SUBST_INVALID_NAME = """
[tox]
envlist = pep8

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {toxinidir!}'
"""

SUBST_INVALID_SECTION = """
[tox]
envlist = pep8

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {[sect@]value}'
"""

SUBST_UNKNOWN_PREDEFINED = """
[tox]
envlist = pep8

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {unknown}'
"""

SUBST_NO_SUCH_SECTION = """
[tox]
envlist = pep8

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {[sect]name}'
"""

SUBST_NO_SUCH_NAME = """
[tox]
envlist = pep8

[sect]
noname = value

[testenv:pep8]
basepython = python3
commands =
  echo 'Bad {[sect]name}'
"""
