"""Tests for the trivtox command-line program."""

# Copyright (c) 2018, 2020, 2022  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

from __future__ import annotations

import pathlib

import pytest

from . import test_data
from . import utils


class TestInvalid:
    """Test various invalid configurations."""

    def test_no_file(self) -> None:
        """Test that trivtox fails if there is no tox.ini file."""
        with utils.chdir_into_temp() as (cwd, _):

            def test_opts(opts: list[str]) -> None:
                with pytest.raises(utils.ErrorOutputError):
                    utils.run_trivtox(opts, path=cwd)

            test_opts([])
            test_opts(["-N"])
            test_opts(["-v"])
            test_opts(["-Nv"])
            test_opts(["-v", "-N"])

    def check_failure(self, cwd: str, conf: str) -> None:
        """Make sure that trivtox fails with the specified config."""
        pathlib.Path("tox.ini").write_text(conf, encoding="UTF-8")
        with pytest.raises(utils.ErrorOutputError):
            utils.run_trivtox([], path=cwd)

    def test_no_tox_section(self) -> None:
        """Test that trivtox fails if there is no tox.envlist."""
        with utils.chdir_into_temp() as (cwd, _):
            self.check_failure(cwd, test_data.NO_TOX_SECTION)
            self.check_failure(cwd, test_data.NO_ENVLIST)

    def test_other_failures(self) -> None:
        """Test that trivtox fails for other reasons."""
        with utils.chdir_into_temp() as (cwd, _):
            self.check_failure(cwd, test_data.NO_ENV)
            self.check_failure(cwd, test_data.NO_COMMAND)
            self.check_failure(cwd, test_data.FAILED_COMMAND)
            self.check_failure(cwd, test_data.FAILED_ONE_OF_TWO)
            self.check_failure(cwd, test_data.INVALID_SETENV)
            self.check_failure(cwd, test_data.SUBST_BAD_LITERAL)
            self.check_failure(cwd, test_data.SUBST_UNCLOSED)
            self.check_failure(cwd, test_data.SUBST_INVALID_NAME)
            self.check_failure(cwd, test_data.SUBST_INVALID_SECTION)
            self.check_failure(cwd, test_data.SUBST_UNKNOWN_PREDEFINED)
            self.check_failure(cwd, test_data.SUBST_NO_SUCH_SECTION)
            self.check_failure(cwd, test_data.SUBST_NO_SUCH_NAME)


class TestValid:
    """Test that trivtox succeeds on valid configurations."""

    def check_success(
        self,
        cwd: str,
        conf: str,
        found: list[str],
        not_found: list[str],
        args: list[str] | None = None,
    ) -> None:
        # pylint: disable=too-many-arguments
        """Make sure trivtox succeeds with the correct output.

        The output must contain the specified lines.
        The order of the lines is not checked; the output may also
        contain other lines.
        """
        if args is None:
            args = []

        pathlib.Path("tox.ini").write_text(conf, encoding="UTF-8")
        res = utils.run_trivtox(args, path=cwd)
        assert res.exitcode == 0
        for line in found:
            assert line in res.stdout_lines
        for line in not_found:
            assert line not in res.stdout_lines

    def test_success(self) -> None:
        """Test that trivtox actually runs some commands."""
        with utils.chdir_into_temp() as (cwd, _):
            self.check_success(cwd, test_data.ECHOES, ["one", "two", "three"], [])
            self.check_success(cwd, test_data.NO_INTERPOLATION, ["good"], ["bad"])
            self.check_success(cwd, test_data.SETENV, ["good"], [])

    def test_subst_full(self) -> None:
        """Test that all the variables will be expanded."""
        with utils.chdir_into_temp() as (cwd, tempdir):
            good = [
                "top-toxinidir-{tmp}-whee",
                "tox-toxworkdir-{tmp}/obj/.tox-whee",
                "tox-temp_dir-{tmp}/obj/.tmp-whee",
                "sp-lib-{tmp}/scripts/usr/lib/storpool-whee",
                "sp-libpy-{tmp}/scripts/usr/lib/storpool/python-whee",
                "sp-tests-{tmp}/tests-whee",
                "sp-unit-{tmp}/tests/unit-whee",
                "sp-unitpy-{tmp}/tests/unit/python-whee",
                "sp-clean_py-{tmp}/tests/clean-py.sh "
                "{tmp}/scripts/usr/lib/storpool/python "
                "{tmp}/tests/unit/python-whee",
                "literal-stuff-some stuff here-whee",
            ]
            good = [line.format(tmp=tempdir) for line in good]
            self.check_success(cwd, test_data.SUBST_FULL, good, [])

    def test_process_all(self) -> None:
        """Test that trivtox actually runs some commands."""
        with utils.chdir_into_temp() as (cwd, _):
            self.check_success(
                cwd,
                test_data.ECHOES,
                ["one", "two", "three", "four"],
                [],
                args=["-a"],
            )

    def test_process_envlist(self) -> None:
        """Test that trivtox actually runs some commands."""
        with utils.chdir_into_temp() as (cwd, _):
            self.check_success(
                cwd,
                test_data.ECHOES,
                [],
                ["one", "two", "three", "four"],
                args=["-e", "mypy_2"],
            )
            self.check_success(
                cwd,
                test_data.ECHOES,
                ["two", "three"],
                ["one", "four"],
                args=["-e", "mypy_2,mypy_3"],
            )
